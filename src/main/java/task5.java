public class task5 {/**
 * Подсчитать площадь и длину окружности для круга с радиусом R. Радиус задан
 * переменной с именем radius. Вывести результат на консоль.
 * Вывод: в одной строке через пробел, сначала окружность, потом площад ь
 */

public static void main(String[] args)
{
    int radius = 10;
    double lenth=2*Math.PI*radius;
    double area=Math.PI*Math.pow(radius,2D);
    System.out.println("Площадь равна"+area);
    System.out.println("Длина окружности равна"+lenth);


}

}
