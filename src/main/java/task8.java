import java.util.Scanner;

public class task8 {/**
 * Зная скорость распространения звука в воздушной среде, можно вычислить
 * расстояние до места удара молнии по времени между вспышкой и раскатом грома.
 * Зная время в секундах между вспышкой и раскатом грома (константа в программе)
 * вычислите расстояния до места удара молнии и выведите его на экран .
 */

public static void main(String[] args) {
    final float SPEED = 340.29f; // м/с

    Scanner time=new Scanner(System.in);
    System.out.println("Введите время между вспышкой и раскатом грома в секундах");
    boolean isInt= time.hasNextFloat();

    if (isInt)
    {float t = time.nextFloat();
        float distanse =SPEED*t;
        System.out.println("Расстояние в метрах " +distanse );
    }
    else {
        System.out.println("Введите время между вспышкой и раскатом грома в секундах");}


}
}
