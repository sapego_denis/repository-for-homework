public class task7 {
    /**
     * Разработать программу, которая позволит при известном годовом проценте
     * вычислить сумму вклада в банке через два года,
     * если задана исходная величина вклада. Вывести результат вычисления в консоль .
     *
     */

    public static void main(String[] args)
    {
        int depositSumma = 1500;
        int annualPercentage =3;
        double timeInMonth=24; //при капитализации вклада по месяцам и сроком на 2 года.

        double income =Math.pow((1+annualPercentage/100D),timeInMonth)*depositSumma;


        System.out.println("Ваш депозит через два года "+income);
    }
}
