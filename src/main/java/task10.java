public class task10 {
    /**
     * Написать программу расчета идеального веса к росту. В константах хранятся
     * рост (height) и вес (weight), вывести на консоль сообщение, сколько нужно кг
     * набрать или сбросить (идеальный вес = рост - 110).
     * Вывод: положительное число если надо набрать вес, отрицательное если похудеть и ноль если вес идеальный
     */

    public static void main(String[] args)
    {
        int height = 198;
        int weight = 95;
        float idealweight=height-110;
        float difrense=weight-idealweight;
        if (difrense>0) {
            System.out.println("вам нужно набрать"+difrense);
        }
        if (difrense<0) {
            System.out.println("вам нужно сбросить"+difrense);
        }
        if (difrense==0) {
            System.out.println("Ваш вес идеален"+difrense);
        }
    }

}