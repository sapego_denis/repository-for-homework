

public class task11 {
    /**

     * У Деда Мороза есть часы, которые в секундах показывают сколько осталось до
     * каждого Нового Года. Так как Дед Мороз уже человек в возрасте, то некоторые
     * математические операции он быстро выполнять не в состоянии. Помогите Деду
     * Морозу определить сколько полных дней, часов, минут и секунд осталось до
     * следующего Нового Года, если известно сколько осталось секунд, т. е.
     * Разложите время в секундах на полное количество дней, часов, минут и секунд.
     * Выведите результат на консоль.
     */

    public static void main(String[] args)
    {
        int SECONDS_TO_NEW_YEAR = 62;
        int days=SECONDS_TO_NEW_YEAR/86400;
        int hours=(SECONDS_TO_NEW_YEAR%86400)/3600;
        int minutes=(SECONDS_TO_NEW_YEAR%86400%3600)/60;
        int seconds =SECONDS_TO_NEW_YEAR%86400%3600%60%60;
        System.out.println(days+"/"+hours+"/"+minutes+"/"+seconds);


    }
}
