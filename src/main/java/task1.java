public class task1 {

    /**
     * В переменных х и y хранятся два натуральных числа. Создайте программу,
     * выводящую на консоль:
     * • Результат целочисленного деления x на y • Остаток от деления x на y.
     * • Квадратный корень  x
     *
     */

    public static void main(String[] args)
    {
        int x = 5;
        int y = 3;
        System.out.println("Целочисленное деление x/y ="+x/y);
        System.out.println("Остаток от деления x/y ="+x%y);
        System.out.println("Квадратный корень x ="+Math.sqrt(x));

    }

}
