public class task9 {/**
 * Проверить, имеет ли число в переменной типа float вещественную часть.
 * Например, числа 3.14 и 2.5 – имеют вещественную часть, а числа 5.0 и 10.0 – нет .
 */

public static void main(String[] args)
{
    final float testFloat = 3.14f;
    int round= (int)testFloat;
    float fraction=testFloat-round;
    if (fraction !=0){
        System.out.println("Число имеет дробную часть");}
    else {
        System.out.println("Число не имеет дробную часть");}
}
}
