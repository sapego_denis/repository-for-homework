public class task3 {
    /**
     * В переменной n хранится вещественное число с ненулевой дробной частью.
     * Создайте программу, округляющую число n до ближайшего целого и выводящую
     * результат округления на экран .
     *
     */

    public static void main(String[] args)
    {
        float n = 31.28f;

        System.out.println(Math.round(n));
    }
}
