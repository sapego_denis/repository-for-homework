
public class task6 {

    /**
     * Есть прямоугольник у которого известна ширина width и высота height,
     * найти и вывести на консоль периметр и площадь заданного
     * прямоугольника. Высота и ширина прямоугольника должна задаваться константными
     * переменными в коде программы.
     * Вывод: сначала периметр, через пробел площад ь
     *
     */

    public static void main(String[] args)
    { final int width = 10;
        final   int height = 3;

        double perimetr = (width+height)*2;
        double area = width*height;
        System.out.print(perimetr);
        System.out.print (" ");
        System.out.print(area);


    }
}
