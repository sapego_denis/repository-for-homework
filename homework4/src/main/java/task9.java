import java.util.Scanner;

public class task9 {
    /**
     * Определить имеют ли чисел M и N разные знаки. Исполь-
     * зуя только побитовые и условные операторы.
     */
    public static void main(String[] args) {
        System.out.println("Input the number N");
        Scanner numberN = new Scanner(System.in);
        boolean isInt1 = numberN.hasNextInt();
        Scanner numberM = new Scanner(System.in);
        System.out.println("Input the number M");
        boolean isInt2 = numberM.hasNextInt();


        if (isInt1 & isInt2) {
            int numN = numberN.nextInt();
            int numM = numberM.nextInt();

            if ((numM ^ numN) >= 0) {
                System.out.println("numbers have the same sign");
            } else {
                System.out.println("numbers are different in sign");
            }

        } else {
            System.out.println("Incorrect input");

        }


    }
}
