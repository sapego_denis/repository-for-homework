import java.util.Scanner;

public class task2 {
    /** Вывести на консоль 2 в степени n. Для вычисления использовать только побитовые операции.

     */
    public static void main(String[] args) {
        Scanner degree = new Scanner (System.in);
        System.out.println("Input degree");
        boolean isInt=degree.hasNextInt();
        if (isInt){
            int degreeN=degree.nextInt();
            int numberNew= 1<<degreeN;
            System.out.println("2 in "+degreeN+"th degree is " +numberNew );
        }
        else {
            System.out.println("Incorrect input");

        }



    }
}
