import java.util.Scanner;

public class task3 {
    /**Установить i-й бит числа N равным 1. Вывести результат
     на консоль в двоичном виде.
     */
    public static void main(String[] args) {
        System.out.println("Input the number N");
        Scanner numberN = new Scanner (System.in);
        boolean isInt1=numberN.hasNextInt();
        Scanner xthByte = new Scanner (System.in);
        System.out.println("Input xth  byte for N");

        boolean isInt2=xthByte.hasNextInt();


        if (isInt1 & isInt2){
            int num1=numberN.nextInt();
            int byteNumber=xthByte.nextInt();
            System.out.println("The number before operation   "+Integer.toBinaryString(num1));
            System.out.println("The number after operation   "+Integer.toBinaryString(num1| (1<<byteNumber)));


        }
        else {
            System.out.println("Incorrect input");

        }



    }
}
