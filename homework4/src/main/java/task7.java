import java.util.Scanner;

public class task7 {
    /**Определить значение i-го бита числа N. Вывести резуль-
     тат на консоль в двоичном виде.
     */
    public static void main(String[] args) {
        System.out.println("Input the number N");
        Scanner numberN = new Scanner (System.in);
        boolean isInt1=numberN.hasNextInt();
        Scanner xthByte = new Scanner (System.in);
        System.out.println("Input x-th  byte for N");
        boolean isInt2=xthByte.hasNextInt();


        if (isInt1 & isInt2){
            int num1=numberN.nextInt();
            int byteNumber=xthByte.nextInt();
            if ((num1 & (1<<byteNumber))==1) {
                System.out.println("In number N the x-th byte is 1 ");
            }
            else {
                System.out.println("In number N the x-th byte is  0 ");
            }
            System.out.println("The number is    "+Integer.toBinaryString(num1));

        }
        else {
            System.out.println("Incorrect input");

        }



    }
}


