import java.util.Scanner;

public class task10 {
    /**Найти и вывести на консоль минимальное из двух чисел
     M и N, используя только побитовые операции.
     */
    public static void main(String[] args) {
        System.out.println("Input the number N");
        Scanner numberN = new Scanner (System.in);
        boolean isInt1=numberN.hasNextInt();
        Scanner numberM = new Scanner (System.in);
        System.out.println("Input the number M");
        boolean isInt2=numberM.hasNextInt();


        if (isInt1 & isInt2){
            int numN=numberN.nextInt();
            int numM=numberM.nextInt();
            int min =numN&((numN-numM)>>31)|numM & (~(numN-numM)>>31);
            int max = numM&((numN-numM)>>31)|numN & (~(numN-numM)>>31);
            System.out.println("The minimal number is  "+ min+ "  "+Integer.toBinaryString(min));
            System.out.println("The max number is   "+ max+ "    "+Integer.toBinaryString(max));


        }
        else {
            System.out.println("Incorrect input");

        }



    }
}




