import java.io.IOException;
import java.util.Scanner;

public class task1 {
    /**Обнулить бит в нулевом разряде числа N. Остальные
    *биты не должны изменить свое значение. Вывести на консоль в двоичном виде
*/
    public static void main(String[] args) throws IOException {


        Scanner numberN = new Scanner(System.in);
        System.out.println("Input the number N");
        boolean isInt=numberN.hasNextInt();
        if (isInt){
            int i =1;
            int numberNew= numberN.nextInt();
            System.out.println("The number before operation"+Integer.toBinaryString(numberNew&(numberNew^1)));
            System.out.println("The number after operation"+Integer.toBinaryString(numberNew));
        }
        else
        { System.out.println("Incorrect input");}




    }

}
